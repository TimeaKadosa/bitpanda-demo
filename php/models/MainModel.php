<?php 

class MainModel
{

	protected $curl;
	protected $response;

	protected $url_all = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest";
	protected $url_one = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/info";
	
	public function __construct(){ }

	protected function connect($parameters, $url) {

		$headers = [
		  'Accepts: application/json',
		  'X-CMC_PRO_API_KEY: ' . API_KEY
		];

		$qs = http_build_query($parameters);
		$request = "{$url}?{$qs}";

		$this->curl = curl_init(); 
		# cURL options
		curl_setopt_array($this->curl, array(

		  CURLOPT_URL => $request,            
		  CURLOPT_HTTPHEADER => $headers,     
		  CURLOPT_RETURNTRANSFER => 1 
		));

		$this->response = curl_exec($this->curl); 

	}

	public function getByID($id)
	{

		$parameters = [
		  'id' => $id
		];

		$this->connect($parameters, $this->url_one);

		$result = json_decode($this->response);

		# close curl session
		curl_close($this->curl); 
		
		return $result;
	}

	function getAll($start, $limit){

		$parameters = [
		  'start' => $start,
		  'limit' => $limit,
		  'convert' => 'USD'
		];

		$this->connect($parameters,$this->url_all);

		$result = json_decode($this->response);

		# close curl session
		curl_close($this->curl); 

		return $result;
	}
}
