<?php

use PHPRouter\RouteCollection;
use PHPRouter\Router;
use PHPRouter\Route;

$collection = new RouteCollection();

# ===== GET ===== #
$collection->attachRoute(new Route('/list-coins', array(
    
    '_controller' => 'ViewController::listCoins',
    'methods' => 'GET'

)));

$collection->attachRoute(new Route('/list/paginate', array(
    
    '_controller' => 'MainController::listPaginate',
    'methods' => 'GET'

)));

$collection->attachRoute(new Route('/list/:page', array(
    
    '_controller' => 'MainController::getList',
    'methods' => 'GET'

)));

$collection->attachRoute(new Route('/feedPopup/:id', array(
    
    '_controller' => 'MainController::feedPopup',
    'methods' => 'GET'

)));


# ===== POST ===== #
// $collection->attachRoute(new Route('/someRoute', array(

//     '_controller' => 'SomeController::someFunction',
//     'methods' => 'POST'
// )));

$router = new Router($collection);

$route = $router->matchCurrentRequest();
# route was not provided, show error
if(!$route){

    $view = new ViewController();
    $view->showView('page404');

}

