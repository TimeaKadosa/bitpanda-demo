<?php 

class ViewController{

    public function __construct() {
            $this->Model = new MainModel();
    }

    public function showView($view, $aData = null) {

        include 'views/partials/header.php';

        if (file_exists('views/' . $view . '.php')) {
                include 'views/' . $view . '.php';	
        }
        else {
                include 'views/page404.php';	
        }

        include 'views/partials/footer.php';

        return true;

    }

    public function listCoins (){
        $this->showView('list');
    }
}