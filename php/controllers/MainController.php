<?php 

class MainController {

	public function __construct() {

		$this->Model = new MainModel();
		$this->View = new ViewController();
	}

	public function getList($page) {

		# get 30 per page (can be defined dynamically as well)
		$limit = 30;
		$pageNr = substr($page,5);
		$pageNr = $page;

		if(is_numeric($page)){
            # calculate the offset
            if($pageNr != 1){
                $start = ($pageNr - 1)*$limit;
            }
            else{
                $start = 1;
            }

            $DataResult = $this->Model->getAll($start, $limit);

            $aData = [
                'DataResult' => $DataResult->data
            ];
            
            echo json_encode($DataResult->data);
		}
		else{
            $this->View->showView('page404', $aData);	
		}
	}

	public function listPaginate(){

		$resultAll = $this->Model->getAll(1, 3000);
		$PaginateTotal = count($resultAll->data);

		echo json_encode($PaginateTotal);
	}



	public function feedPopup($id){

		$result = $this->Model->getByID($id);
		echo json_encode($result->data);
	}

}