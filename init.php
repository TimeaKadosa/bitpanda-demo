<?php
# make the debugging easier and faster to read
function debug($data){
	echo "<pre>";
	var_dump($data);
	echo "</pre>";
}
/*
	=== Base Settings ===
*/
mb_internal_encoding("UTF-8");
mb_http_output("UTF-8");
ini_set('default_charset', 'UTF-8');
date_default_timezone_set('Europe/Amsterdam');

# 'On' only for debugging purpose 
error_reporting(E_ALL); ini_set('display_errors', 'On');

/* 
	========================================================================================
  		CoinMarketCap
	========================================================================================
*/
define('API_KEY', 'd76236eb-d217-461a-ae94-6289c999cf58');

define ('DOC_ROOT', __DIR__);
set_include_path(DOC_ROOT);


/* 
    ========================================================================================
			INCLUDE MODELS/CONTROLLERS
    ========================================================================================
*/
include ('include.classes.php');

/* 
	========================================================================================
  		INCLUDE ROUTING SYSTEM
    ========================================================================================
*/
include ('services/PHP-Router/Route.php');
include ('services/PHP-Router/RouteCollection.php');
include ('services/PHP-Router/Router.php');

require 'php/appRouter.php';