$(document).ready(function () {

    if (window.location.pathname == "/list-coins") {

        $.get("/list/paginate", function (data) {
            $(".pagination").pagination({
                items: data,
                itemsOnPage: 30,
                cssStyle: 'light-theme',
                hrefTextPrefix: '#',
                onInit: buildTableOnLoad
            });
        });
    }

    function buildTableOnLoad() {
        // feed the table when reload or first load the page
        $.get("/list/" + 1, function (data) {

            let obj = jQuery.parseJSON(data);
            buildTable(obj);

            // show the table smoothly
            setTimeout(function () {
                $("#resultList").fadeIn();
            }, 500);

        });
    }

    function buildTable(data) {

        // clean the table
        $('#DataTableBody').empty();

        // fedd with new data
        $.each(data, function (index, value) {

            let elemTd = '<tr class="row_item" id="data_' + value.id + '" data-toggle="modal" data-target="#coinModal"><td>' + value.name + '</td>';
            elemTd += '<td>' + value.symbol + '</td></tr>';
            $('#DataTableBody').append(elemTd);

        });
    }


    // feed the table when clicking on page number
    $(document).on('click', '.page-link', function (e) {

        let strHref = $(this).attr('href');
        let parts = strHref.split('#');
        let pageNr = parts[1];

        $.get("/list/" + pageNr, function (data) {

            let obj = jQuery.parseJSON(data);
            buildTable(obj);

        });
    });


    // feed the modal with dynamic data
    $(document).on('click', '.row_item', function (e) {

        let item_id = $(this).attr('id').substring(5);
        $.get("/feedPopup/" + item_id, function (data) {

            let obj = $.parseJSON(data);
            let logo = obj[item_id].logo;
            let description = obj[item_id].description;
            let name = obj[item_id].name;
            
            $("#coinModal img").attr('src', logo);
            $("#coinModalLabel .title").text(name);
            $("#coinModal .modal-body").text(description);
        });
    });

    // clean up the content after close 
    $('#coinModal .close, #coinModal .closeBtn').on('click', function () {

        $("#coinModal img").attr('src', '');
        $("#coinModalLabel .title").text('');
        $("#coinModal .modal-body").empty();

    });
});