<!DOCTYPE html>
<html>
    <head>
        <title>Bitpanda Assignment</title>
        <!-- Mobile View -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Include jQuery -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        
        <!-- Include Custom Style / JS -->
        <link href="/css/style.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/js/main.js"></script>

        <!-- Pagination jQuery -->
        <script type="text/javascript" src="/js/jquery.simplePagination.js"></script>
        <link type="text/css" rel="stylesheet" href="/css/simplePagination.css"/>

    </head>
    <body>


    	<div id="main">
    	
    	<h3>Bitpanda Assignment</h3>
        <hr>
        <div id="content-wrapper" >