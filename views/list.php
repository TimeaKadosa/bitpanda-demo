<div id="coinList">
    <ul class="pagination"></ul>

    <table id="resultList" class="table table-striped table-bordered table-hover">

        <thead>
            <tr>
                <th>Name</th>
                <th>Symbol</th>
            </tr>
        </thead>

        <tbody id="DataTableBody"></tbody>

        <tfoot>
            <tr>
                <th>Name</th>
                <th>Symbol</th>
            </tr>
        </tfoot>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="coinModal" tabindex="-1" role="dialog" aria-labelledby="coinModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="coinModalLabel"><img src=""> <span class="title"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body"></div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary closeBtn" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>