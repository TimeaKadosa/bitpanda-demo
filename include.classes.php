<?php
	/**
	 * Register class autoloader using an anonymous function (PHP >= 5.3.0)
	 */

	spl_autoload_register(function ($sClassname) {

		# Get the last part of a namespaced classname
		# Set the classname to require accordingly
		$parts = explode('\\', $sClassname);
		$sClassname = end($parts);
		# Set the path to include the class from

		if (stripos($sClassname, 'controller') !== false) {

			$sIncludePath = 'php/controllers/';

		} elseif (stripos($sClassname, 'model') !== false) {

			$sIncludePath = 'php/models/';
		} 

		# Include the class
		if (isset($sIncludePath)) {
			require_once ($sIncludePath.$sClassname.'.php');
		}
	});

